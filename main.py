# -*- coding: utf-8 -*-

from __future__ import absolute_import
import datetime

from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.stacklayout import StackLayout
from kivy.uix.button import Button
from kivy.properties import ObjectProperty
from kivy.properties import ListProperty
from kivy.properties import StringProperty
from kivy.properties import OptionProperty
from kivy.properties import NumericProperty
from kivy.properties import BooleanProperty
from kivy.metrics import dp

from kivy.uix.behaviors.compoundselection import CompoundSelectionBehavior

from kivy.uix.listview import ListItemButton

from kivy.uix.recycleview.layout import LayoutSelectionBehavior
from kivy.uix.behaviors.focus import FocusBehavior
from kivy.uix.recycleboxlayout import RecycleBoxLayout

from kivy.uix.screenmanager import ScreenManager, Screen

from tasklib.backends import TaskWarrior
from tasklib.task import Task
from tasklib.serializing import local_zone
from sys import exit

from kivymd.theming import ThemeManager
from kivy.uix.image import Image
from kivymd.list import ILeftBody, IRightBodyTouch
from kivymd.selectioncontrols import MDCheckbox
from kivymd.list import TwoLineAvatarIconListItem
from kivymd.button import MDFlatButton
import kivymd.snackbar as Snackbar
from kivymd.dialog import MDDialog
from kivymd.list import OneLineRightIconListItem
from kivymd.list import ThreeLineRightIconListitem
from kivymd.list import TwoLineRightIconListItem
from kivymd.date_picker import MDDatePicker
from kivymd.time_picker import MDTimePicker
from kivymd.button import MDIconButton
from kivymd.navigationdrawer import NavigationDrawer

from time import localtime
# from itertools import izip


import pytz
MYZONE = pytz.timezone(u'Europe/Berlin')

import locale
# locale.setlocale(locale.LC_ALL, 'fr_FR')
# locale.setlocale(locale.LC_ALL, u'C')

# from pudb.remote import set_trace
# set_trace(term_size=(99,48))
from kivy.utils import platform
from os import environ
from subprocess import Popen
from os.path import expanduser


if platform == 'android':
    # Popen(['chmod', '755', 'usr/bin/task'])
    Popen(['chmod', '755', 'task'])
    Popen(['ls', '-l'])
    environ['LD_LIBRARY_PATH'] = 'libs/android/'
    environ['PATH'] += ':.'

    TW = TaskWarrior(taskrc_location='/sdcard/taskrc', data_location=u'/sdcard/task/', create=True)
else:
    TW = TaskWarrior(taskrc_location=expanduser('~/.task/taskrc'))

# print "LD_PATH_____LIBRARY"
# print environ['LD_LIBRARY_PATH']

TASK_STATUS = [u'pending', u'deleted', u'completed', u'waiting', u'recurring']


class TwuiTwuiRoot(ScreenManager):
    pass


class ProjectPopup(MDDialog):
    projects_list = ListProperty([])

    def __init__(self, *args, **kwargs):
        super(ProjectPopup, self).__init__(*args, **kwargs)
        self.add_action_button(u"Back", action=lambda *x: self.dismiss())

    def on_open(self):
        self.generate_projects_list()
        for i in self.projects_list:
            # for i in self.ids.rv.data:
            if i[u'name'] == App.get_running_app().root.get_screen(
                    u'taskscreen').project:
                self.ids.rv_box.select_node(self.projects_list.index(i))

    def on_dismiss(self):
        del self.projects_list[:]

    def generate_projects_list(self):
        projects_list = []
        plist = App.get_running_app().tw.execute_command(
            [u'_projects'],
            allow_failure=False,
            return_all=True)[0]
        for i in plist:
            projects_list.append({u'name': i, u'selectable': True})
        self.projects_list = list(projects_list)

    def on_projects_list(self, instance, value):
        self.projects_list = sorted(value, key=lambda project: project[u'name'])
        for i in value:
            if i[u'name'] == App.get_running_app().root.get_screen(
                    u'taskscreen').project:
                self.ids.rv_box.select_node(value.index(i))
            else:
                self.ids.rv_box.deselect_node(value.index(i))


class ProjectsRecycleBoxLayout(FocusBehavior, LayoutSelectionBehavior,
                               RecycleBoxLayout):
    pass


class MDProjectLine(OneLineRightIconListItem):
    name = StringProperty()


class ProjectSelectionCheckbox(IRightBodyTouch, MDCheckbox):
    value = StringProperty(u'')

    def on_value(self, instance, value):
        if App.get_running_app().root.get_screen(
                u'taskscreen').project == self.value:
            self.state = u'down'
        else:
            self.state = u'normal'

    def on_press(self):
        App.get_running_app().root.get_screen(
            u'taskscreen').project = self.parent.parent.text
        App.get_running_app().projects_popup.dismiss()


class AddProjectPopup(MDDialog):
    project = StringProperty(u'')

    def __init__(self, *args, **kwargs):
        super(AddProjectPopup, self).__init__(*args, **kwargs)
        self.add_action_button(u"Ok", action=lambda *x: self.validate())
        self.add_action_button(u"Dismiss", action=lambda *x: self.dismiss())

    def validate(self, *args):
        validated = False
        plist = App.get_running_app().projects_popup.projects_list

        if len(self.ids.name.text) == 0:
            self.ids.name.message = u'Project must have at least one character'
            self.ids.name.error = True
        elif self.ids.name.text in [i[u'name'] for i in plist]:
            self.ids.name.message = u'Project already exist'
            self.ids.name.error = True
        else:
            validated = True
            self.ids.name.error = False

        if validated:
            plist.append({u'name': self.ids.name.text, u'selectable': True})
            self.dismiss()

    def on_open(self):
        # print self.ids.name
        self.ids.name.bind(on_text_validate=self.validate,
                           on_focus=self.validate)


class TagPopup(MDDialog):
    pass


class TagsRecycleBoxLayout(RecycleBoxLayout):
    def on_parent(self, instance, value):
        # print u'INSTANCE : {}'.format(instance)
        # print u'VALUE : {}'.format(value)
        if App.get_running_app().root.get_screen(u'taskscreen').task:
            for i in App.get_running_app().tags_list_data:
                if i[u'name'] in App.get_running_app().root.get_screen(
                        u'taskscreen').tags:
                    self.select_node(self.tags_list.index(i))


class MDTagLine(OneLineRightIconListItem):
    name = StringProperty()


class MDTagLineFilter(OneLineRightIconListItem):
    name = StringProperty()


class TagSelectionCheckbox(IRightBodyTouch, MDCheckbox):
    value = StringProperty(u'')

    def on_value(self, instance, value):
        if self.value in App.get_running_app().root.get_screen(
                u'taskscreen').tags:
            self.state = u'down'
        else:
            self.state = u'normal'

    def on_press(self):
        tag = self.parent.parent.text
        task_tags = App.get_running_app().root.get_screen(u'taskscreen').tags
        if tag in task_tags:
            task_tags.remove(tag)
        else:
            task_tags.append(tag)


class TagSelectionCheckboxFilter(IRightBodyTouch, MDCheckbox):
    u"""This class manage actions for filtering on tags."""

    def __init__(self, **kwargs):
        u"""Set filter_tags variable for DRY"""
        super(TagSelectionCheckboxFilter, self).__init__(**kwargs)
        self.filter_tags = App.get_running_app(
        ).root.ids.dashboardscreen.ids.tasks_list.filter_tags

    def on_active(self, instance, value):
        u"""Append or remove tag name from the filter_tags ListProperty defined
        in TasksList class.
        """
        tag = self.parent.parent.text
        if value is True:
            self.filter_tags.append(tag)
        else:
            self.filter_tags.remove(tag)

        # if len(self.filter_tags) > 0:
        #     App.get_running_app().sliding_panel.ids.tags.background_color = self.theme_cls._get_primary_dark()
        # else:
        #     App.get_running_app().sliding_panel.ids.tags.background_color = self.theme_cls._get_primary_color()


class AddTagPopup(MDDialog):
    u"""This class allow to create new tag.
    The new tag will only be saved if is used in the current task AND
    the task is saved.
    """
    project = StringProperty(u'')

    def __init__(self, *args, **kwargs):
        super(AddTagPopup, self).__init__(*args, **kwargs)
        self.add_action_button(u"Ok", action=lambda *x: self.validate())
        self.add_action_button(u"Dismiss", action=lambda *x: self.dismiss())

    def validate(self, *args):
        u"""Validate the new project name.
        The following rules are applied:
            - project name must have at least one character
            - project name must not already exist
        """
        validated = False
        tlist = App.get_running_app().tags_list_data

        if len(self.ids.name.text) == 0:
            self.ids.name.message = u'Project must have at least one character'
            self.ids.name.error = True
        elif self.ids.name.text in [i[u'name'] for i in tlist]:
            self.ids.name.message = u'Project already exist'
            self.ids.name.error = True
        else:
            validated = True
            self.ids.name.error = False

        if validated:
            tlist.append({u'name': self.ids.name.text, u'selectable': True})
            self.dismiss()

    def on_open(self):
        self.ids.name.bind(on_text_validate=self.validate,
                           on_focus=self.validate)


class TaskScreen(Screen):
    default_date = datetime.datetime(2030,
                                     12,
                                     31,
                                     23,
                                     59,
                                     59,
                                     tzinfo=pytz.timezone(u"Europe/Paris"))
    description = StringProperty(u'', rebind=True)
    status = StringProperty()
    project = StringProperty(u'', rebind=True)
    priority = OptionProperty(u'', options=[u"H", u"M", u"L", u""], rebind=True)
    entered = ObjectProperty(getter=u'dedede', baseclass=datetime.datetime)
    due_date = ObjectProperty(None, baseclass=datetime.date, allownone=True)
    due = ObjectProperty(default_date,
                         baseclass=datetime.datetime,
                         rebind=True,
                         allownone=True)
    due_time = ObjectProperty(None, baseclass=datetime.time, allownone=True)
    last_modified = ObjectProperty(getter=u'dedede',
                                   baseclass=datetime.datetime)
    tags = ListProperty([], rebind=True)
    uuid = StringProperty()
    urgency = NumericProperty()
    current_task = StringProperty()
    mode = StringProperty(u'edit')
    task = ObjectProperty(None, allownone=True)

    def on_priority(self, instance, value):
        App.get_running_app().priority_dialog.dismiss()

    def on_leave(self, *args, **kwargs):
        self.description = u''
        self.project = u''
        self.priority = u''
        self.due = None
        self.due_date = None
        self.due_time = None
        self.mode = u'edit'
        del self.tags[:]
        # App.get_running_app().tags_list.rv_box.clear_selection()
        self.task = None

    def on_enter(self, *args, **kwargs):
        if self.mode == u'create':
            self.task = Task(App.get_running_app().tw)
        else:
            self.get_task_from_server(self.current_task)
            # self.ids.description.hint_anim_in.start(self.ids.description)

    def on_task(self, instance, value):
        # print u'TASKKK : {}'.format(self.task)
        # Space trick !! Need to be cleaned
        if self.task:
            if self.task['uuid'] is not None:
                self.fill_fields_with_task_details()
            else:
                pass

    def open_priority_dialog(self):
        App.get_running_app().priority_dialog.open()

    def get_task_from_server(self, uuid):
        self.task = App.get_running_app().tw.tasks.get(uuid=uuid)

    def fill_fields_with_task_details(self):
        for i, j in self.task._data.items():
            if i != u'id':
                setattr(self, i, j)
        if self.task[u'due']:
            self.due_date = self.task[u'due'].date()
            self.due_time = self.task[u'due'].time()

    def save(self, **kwargs):
        self.task[u'description'] = self.description
        self.task[u'project'] = self.project
        self.task[u'priority'] = self.priority
        if self.due_date:
            self.task[u'due'] = self.due
        self.task[u'tags'] = self.tags
        self.task.save()

    def set_due_date(self, date_obj):
        # print u'ENTER DUE DATE'
        self.due_date = date_obj
        if self.due_time:
            self.due = datetime.datetime(date_obj.year, date_obj.month,
                                         date_obj.day, self.due_time.hour,
                                         self.due_time.minute)
        else:
            self.due = datetime.datetime(date_obj.year, date_obj.month,
                                         date_obj.day)

        # print self.due

    def show_date_picker(self):
        if self.due_date:
            pd = self.due_date
            try:
                MDDatePicker(self.set_due_date, pd.year, pd.month,
                             pd.day).open()
            except AttributeError:
                MDDatePicker(self.set_due_date).open()
        else:
            MDDatePicker(self.set_due_date).open()

    def get_time_picker_data(self, instance, time):
        self.due_time = time
        if self.due_date:
            self.due = datetime.datetime(
                self.due_date.year, self.due_date.month, self.due_date.day,
                time.hour, time.minute)
        else:
            self.due = datetime.datetime(
                self.default_date.year, self.default_date.month,
                self.default_date.day, time.hour, time.minute)

    def show_time_picker(self):
        self.time_dialog = MDTimePicker()
        self.time_dialog.bind(time=self.get_time_picker_data)
        if self.due_time:
            try:
                self.time_dialog.set_time(self.due_time)
            except AttributeError:
                pass
        self.time_dialog.open()

    def set_due_time(self, time_obj):
        self.due_time = time_obj

    def reset_due(self):
        self.due = None
        self.due_date = None
        self.due_time = None


class SortButton(MDFlatButton):
    def on_release(self):
        App.get_running_app().root.get_screen(
            u'dashboardscreen').ids.tasks_list.sort_key = self.sort_key


class SortOrderBoxLayout(BoxLayout):
    pass


class DashBoardScreen(Screen):
    def on_enter(self):
        if len(self.ids) > 0:
            self.ids.tasks_list.apply_filters()
            App.get_running_app().generate_tags_list()
            # self.ids['bl'].add_widget(FilterDrawer())
            # self.add_widget(FilterDrawer())

    sort_menu_items = [
        {u'viewclass': u'SortOrderBoxLayout'},
        {u'viewclass': u'SortButton',
         u'text': u'Due Date',
         u'sort_key': u'due',
         u'group': u'sort_buttons'},
        {u'viewclass': u'SortButton',
         u'text': u'Description',
         u'sort_key': u'description',
         u'group': u'sort_buttons'},
        {u'viewclass': u'SortButton',
         u'text': u'Priority',
         u'sort_key': u'priority',
         u'group': u'sort_buttons'},
    ]

    filter_menu_items = [
        {u'viewclass': u'FilterByDescriptionButton',
         u'text': u'Description',
         u'sort_key': u'description',
         u'group': u'filter_buttons'},
        {u'viewclass': u'FilterButton',
         u'text': u'Projet',
         u'filter_key': u'due',
         u'group': u'filter_buttons'},
        {u'viewclass': u'FilterByTagButton',
         u'text': u'Tag',
         u'sort_key': u'tag',
         u'group': u'filter_buttons'},
        {u'viewclass': u'FilterButton',
         u'text': u'Due Date',
         u'filter_key': u'due',
         u'group': u'filter_buttons'},
    ]


from kivymd.slidingpanel import SlidingPanel


class FilterPanel(SlidingPanel):

    pass


class FilterDrawer(NavigationDrawer):
    pass


class FilterBox(BoxLayout):
    pass


from kivymd.accordion import MDAccordionItem


class FilterAccordionItem(MDAccordionItem):
    pass


class FilterByDescriptionButton(MDFlatButton):
    def on_release(self):
        App.get_running_app().root.get_screen(
            u'dashboardscreen').ids.filter_box.add_widget(DescriptionFilterBox(
            ))


class FilterByTagButton(MDFlatButton):
    def on_release(self):
        App.get_running_app().root.get_screen(
            u'dashboardscreen').ids.tags_filter_stack_box.add_tags_buttons()


class FilterButton(MDFlatButton):
    def on_release(self):
        App.get_running_app().root.get_screen(
            u'dashboardscreen').ids.tasks_list.sort_key = self.sort_key


class TagsFilterButton(Button):
    pass


class DescriptionFilterBox(BoxLayout):
    pass

from kivy.clock import Clock
class TasksList(BoxLayout):
    tasks_list_formated = ListProperty([], rebind=True)
    tasks_list_data = ListProperty([], rebind=True)
    tasks_list_buffer = ListProperty([])
    filter_tags = ListProperty([], rebind=True)
    filter_description = StringProperty(u'')
    filter_priority = StringProperty(u'')
    filter_status = StringProperty(u'')
    tasks_list = ListProperty([], rebind=True)
    sort_key = StringProperty('uuid')
    sort_order = OptionProperty(u'asc', options=[u'asc', u'desc'])

    def __init__(self, *args, **kwargs):
        # Set default value for 'status'
        # This init the first list by changing filter_status
        Clock.schedule_once(self.set_default_status_pending, .1)
        Clock.schedule_once(self.set_filter_button_background, 1)
        super(TasksList, self).__init__(*args, **kwargs)
        
    def set_default_status_pending(self, *args):
        self.filter_status = u'pending'

    def set_default_status_pending(self, *args):
        self.filter_status = u'pending'

    def apply_filters(self):
        self.retrieve_tasks_list_from_server()
        tmp_list = []
        tags_list = []
        description_list = []
        priority_list = []
        status_list = []
        filters_list = [tags_list, description_list, priority_list, status_list]
        if self.filter_tags is not []:
            for i in self.tasks_list:
                if set(self.filter_tags).issubset(set(i[u'tags'])):
                    tags_list.append(i)
        if self.filter_description is not u'':
            for i in self.tasks_list:
                if i[u'description'].startswith(self.filter_description):
                    description_list.append(i)
        if self.filter_priority is not u'':
            for i in self.tasks_list:
                if i[u'priority'] == self.filter_priority:
                    priority_list.append(i)
        if self.filter_status is not u'':
            for i in self.tasks_list:
                if i[u'status'] == self.filter_status:
                    status_list.append(i)

        tmp_list = [i for i in filters_list if len(i) > 0]
        if len(tmp_list) != 0:
            self.tasks_list = list(set(self.tasks_list).intersection(*
                                                                     tmp_list))
        else:
            pass
        self.set_filter_button_background()

    def set_filter_button_background(self, *args):
        filters_list = [self.filter_description, self.filter_priority, self.filter_status,
                self.filter_tags]
        if all(len(i) < 1 for i in filters_list):
            App.get_running_app().root.ids.dashboardscreen.ids.filter_button.opposite_colors = False
        else:
            App.get_running_app().root.ids.dashboardscreen.ids.filter_button.opposite_colors = True 


    def on_filter_priority(self, instance, value):
        if len(value) > 0:
            App.get_running_app(
            ).sliding_panel.ids.priority.background_color = App.get_running_app(
            ).theme_cls._get_primary_dark()
        else:
            App.get_running_app(
            ).sliding_panel.ids.priority.background_color = App.get_running_app(
            ).theme_cls._get_primary_color()
        self.apply_filters()

    def on_filter_tags(self, instance, value):
        if len(value) > 0:
            App.get_running_app(
            ).sliding_panel.ids.tags.background_color = App.get_running_app(
            ).theme_cls._get_primary_dark()
        else:
            App.get_running_app(
            ).sliding_panel.ids.tags.background_color = App.get_running_app(
            ).theme_cls._get_primary_color()

        self.apply_filters()

    def on_filter_description(self, instance, value):
        if len(value) > 0:
            App.get_running_app(
            ).sliding_panel.ids.description.background_color = App.get_running_app(
            ).theme_cls._get_primary_dark()
        else:
            App.get_running_app(
            ).sliding_panel.ids.description.background_color = App.get_running_app(
            ).theme_cls._get_primary_color()
        self.apply_filters()

    def on_filter_status(self, instance, value):
        if len(value) > 0:
            App.get_running_app(
            ).sliding_panel.ids.status.background_color = App.get_running_app(
            ).theme_cls._get_primary_dark()
        else:
            App.get_running_app(
            ).sliding_panel.ids.status.background_color = App.get_running_app(
            ).theme_cls._get_primary_color()
        self.apply_filters()

    def count_tasks(self, value):
        if App.get_running_app().root:
            App.get_running_app(
            ).root.ids.dashboardscreen.ids.toolbar.title = str((len(value)))

    def on_tasks_list(self, instance, value):
        self.count_tasks(value)
        data = []
        # print u'NB TASKS : {}'.format(len(value))
        columns = ['uuid', u'description', u'due', u'priority', u'tags']
        for t in self.tasks_list:
            # self.ids.mdlist.add_widget(MDTaskLine(
            #     description=t['description'],
            #     status=t['status'],
            #     due=t['due']))
            tmp_dict = {}
            for a in columns:
                if a in t._data:
                    tmp_dict[a] = t[a]
                elif a == u'due':
                    tmp_dict[a] = datetime.datetime(
                        2030,
                        12,
                        31,
                        23,
                        59,
                        59,
                        tzinfo=pytz.timezone(u"Europe/Paris"))
                else:
                    tmp_dict[a] = u''
            data.append(tmp_dict)
        self.tasks_list_data = data

    def on_tasks_list_data(self, instance, value):
        self.tasks_list_formated = self.format_tasks_list(value)

    def format_tasks_list(self, data):
        for i in data:
            for k, v in i.items():
                if hasattr(self, k.lower()):
                    i[k] = getattr(self, k.lower())(v)
                if k == u'priority':
                    if v in (u' ', None):
                        i[k] = u' '
        return data

    def on_sort_key(self, instance, value):
        self.sort_tasks_list(sort_key=value)

    def on_sort_order(self, instance, value):
        self.sort_tasks_list(self.sort_key)

    def retrieve_tasks_list_from_server(self):
        self.tasks_list = App.get_running_app().tw.tasks.all()

    def sort_tasks_list(self, sort_key='uuid'):
        try:
            from itertools import izip
        except ImportError:
            izip = zip
            pass
        # Python3 use izip
        # tmp_list = list(izip(self.tasks_list_data, self.tasks_list_formated))
        # Python2 use zip
        tmp_list = list(zip(self.tasks_list_data, self.tasks_list_formated))
        tmp_list.sort(key=lambda i: i[0][sort_key])
        self.tasks_list_formated = [x for x, y in tmp_list]
        if self.sort_order == u'desc':
            self.tasks_list_formated.reverse()

    def calculate_task_age(self, task):
        entry = task[u'entry']
        now = local_zone.localize(datetime.datetime.now())
        age = u"{}d".format(unicode((now - entry).days))
        return age


class MDTaskLine(TwoLineRightIconListItem):
# class MDTaskLine(ThreeLineRightIconListitem):
    uuid = StringProperty('')
    description = StringProperty(u'')
    due = ObjectProperty(
        datetime.datetime(2030,
                          12,
                          31,
                          23,
                          59,
                          59,
                          tzinfo=pytz.timezone(u"Europe/Paris")),
        rebind=True)
    priority = StringProperty(u'')

    def mark_task_done(self):
        task = App.get_running_app().tw.tasks.filter(uuid=self.uuid)
        task[0].done()
        Snackbar.make(u'{} mark as Done'.format(self.description))


class IconDone(IRightBodyTouch, MDIconButton):
    pass


class TaskListHeaderButton(Button):
    name = StringProperty()
    order = NumericProperty(0)


class TaskButton(ListItemButton):
    tasks = ListProperty()


from kivymd.dialog import MDDialog


class MDPriorityDialog(MDDialog):
    def on_open(self):
        priority = App.get_running_app().root.get_screen(u'taskscreen').priority
        if priority:
            # print self.ids
            # print priority
            self.ids[priority].active = True
            self.ids[priority].state = u'down'

    def on_dismiss(self):
        for i, j in self.ids.items():
            if isinstance(j, PrioritySelectionCheckbox):
                j.state = u'normal'


class PrioritySelectionCheckbox(IRightBodyTouch, MDCheckbox):
    value = StringProperty(u'')

    def on_active(self, checkbox, value):
        if value:
            App.get_running_app().root.get_screen(
                u'taskscreen').priority = self.value
        else:
            pass


class MDPriorityDialogFilter(MDDialog):
    pass

class MDStatusDialogFilter(MDDialog):
    pass

class PrioritySelectionCheckboxFilter(IRightBodyTouch, MDCheckbox):
    value = StringProperty(u'')

    def on_active(self, checkbox, value):
        if value:
            App.get_running_app(
            ).root.ids.dashboardscreen.ids.tasks_list.filter_priority = self.value
        else:
            App.get_running_app(
            ).root.ids.dashboardscreen.ids.tasks_list.filter_priority = u''


class StatusSelectionCheckboxFilter(IRightBodyTouch, MDCheckbox):
    value = StringProperty(u'')

    def on_active(self, checkbox, value):
        if value:
            # print self.value
            App.get_running_app(
            ).root.ids.dashboardscreen.ids.tasks_list.filter_status = self.value
        else:
            App.get_running_app(
            ).root.ids.dashboardscreen.ids.tasks_list.filter_status = u''

from kivy.uix.recycleview import RecycleView


class TagsList(RecycleView):
    pass


class TagsListFilter(RecycleView):
    pass


from kivymd.textfields import SingleLineTextField


class DescriptionFilterSingleLineTextField(SingleLineTextField):
    def on_text(self, instance, value):
        App.get_running_app(
        ).root.ids.dashboardscreen.ids.tasks_list.filter_description = self.text


class TwuiTwuiApp(App):
    theme_cls = ThemeManager()
    first_run = 0
    projects_popup = ObjectProperty()
    priority_dialog = ObjectProperty()
    filter_drawer = ObjectProperty()
    sliding_panel = ObjectProperty()
    tags_list_data = ListProperty([])

    def __init__(self, **kwargs):
        super(TwuiTwuiApp, self).__init__(**kwargs)
        self.server_connection()
        self.generate_tags_list()

    def server_connection(self):
        try:
            self.tw = TW
        except:
            exit()

    def build(self):
        self.projects_popup = ProjectPopup()
        self.add_project_popup = AddProjectPopup()

        self.tags_popup = TagPopup()
        self.add_tag_popup = AddTagPopup()

        self.priority_dialog = MDPriorityDialog()

        self.sliding_panel = SlidingPanel()

    def on_priority_dialog(self, instance, value):
        pass
        # print u'ON_PRIORITY_DIAL'

    def generate_tags_list(self):
        tags_list = []
        plist = App.get_running_app().tw.execute_command(
            [u'_tags'],
            allow_failure=False,
            return_all=True)[0]
        for i in plist:
            tags_list.append({u'name': i, u'selectable': True})
        self.tags_list_data = tags_list

    def on_tags_list_data(self, instance, value):
        self.tags_list_data = sorted(value, key=lambda tag: tag[u'name'])

    def attributes_list(self):
        alist = self.tw.execute_command(
            [u'_columns'],
            allow_failure=False,
            return_all=True)[0]
        return alist


if __name__ == u'__main__':
    TwuiTwuiApp().run()
